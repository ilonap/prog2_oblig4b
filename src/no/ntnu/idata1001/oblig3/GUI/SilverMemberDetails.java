package no.ntnu.idata1001.oblig3.GUI;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import no.ntnu.idata1001.oblig3.Logic.Personals;
import no.ntnu.idata1001.oblig3.Logic.SilverMember;

public class SilverMemberDetails {

    public static void display(SilverMember memberToDisplay) {
        Stage dialogBox = new Stage();

        dialogBox.initModality(Modality.APPLICATION_MODAL);
        dialogBox.setTitle("Gold Member");
        dialogBox.setMinWidth(250);
        dialogBox.setMinHeight(250);

        SilverMember member = memberToDisplay;
        Personals personals = member.getPersonals();

        Label memberNo = new Label();
        memberNo.setText("Member Number: " + ((Integer)member.getMemberNo()).toString());

        Label enrolledDate = new Label();
        enrolledDate.setText("Enrolled Date: " + member.getEnrolledDate().toString());

        Label points = new Label();
        points.setText("Points: " + ((Integer)member.getBonusPoints()).toString());

        Label firstName = new Label();
        firstName.setText("First Name: " + personals.getFirstname());

        Label surname = new Label();
        surname.setText("Surname: " + personals.getSurname());

        Label email = new Label();
        email.setText("Email: " + personals.getEMailAddress());

        Button closeButton = new Button("Close");
        closeButton.setOnAction(e -> dialogBox.close());

        VBox layout = new VBox(10);
        layout.getChildren().addAll(memberNo, firstName, surname, points, enrolledDate, email, closeButton);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout);
        dialogBox.setScene(scene);
        dialogBox.showAndWait();
    }
}
