package no.ntnu.idata1001.oblig3.GUI;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import no.ntnu.idata1001.oblig3.Logic.*;

import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;


public class BonusApp extends Application {

    private static final Logger logger = Logger.getLogger(BonusApp.class.getName());

    MemberArchive memberErchive = new MemberArchive();
    TextField firstNameInput;
    TextField surnameInput;
    TextField emailInput;
    TextField passwordInput;
    TableView<BonusMember> table;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Stage window = primaryStage;
        window.setTitle("Bonus Member App");

        fillRegisterWithMembers();

        table = new TableView<>();
        TableColumn<BonusMember, Integer> memberNoColumn = new TableColumn<>("Member No");
        memberNoColumn.setMinWidth(199);
        memberNoColumn.setCellValueFactory(new PropertyValueFactory<>("memberNo"));

        TableColumn<BonusMember, Integer> pointsColumn = new TableColumn<>("Points");
        pointsColumn.setMinWidth(199);
        pointsColumn.setCellValueFactory(new PropertyValueFactory<>("bonusPoints"));

        TableColumn<BonusMember, LocalDate> enrolledDateColumn = new TableColumn<>("Enrollment Date");
        enrolledDateColumn.setMinWidth(200);
        enrolledDateColumn.setCellValueFactory(new PropertyValueFactory<>("enrolledDate"));

        table.setItems(getMembers());
        table.getColumns().addAll(memberNoColumn, pointsColumn, enrolledDateColumn);
        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        firstNameInput = new TextField();
        firstNameInput.setPromptText("First name");
        firstNameInput.setMinWidth(100);

        surnameInput = new TextField();
        surnameInput.setPromptText("Surname");
        surnameInput.setMinWidth(100);

        emailInput = new TextField();
        emailInput.setPromptText("Email");
        emailInput.setMinWidth(100);

        passwordInput = new TextField();
        passwordInput.setPromptText("Password");
        passwordInput.setMinWidth(100);

        Button addButton = new Button("Add");
        addButton.setMinWidth(50);
        addButton.setOnAction(e -> addButtonClicked());

        Button deleteButton = new Button("Delete");
        deleteButton.setOnAction(e -> deleteButtonClicked());

        Button upgradeButton = new Button("Upgrade");
        upgradeButton.setOnAction(e -> upgradeButtonClicked());

        Button detailsButton = new Button("Show Details");
        detailsButton.setOnAction(e -> detailsButtonClicked());

        HBox newMemberBar = new HBox();
        newMemberBar.setPadding(new Insets(10, 10, 10, 10));
        newMemberBar.setSpacing(10);
        newMemberBar.getChildren().addAll(firstNameInput, surnameInput, emailInput, passwordInput, addButton);

        HBox buttonsBar = new HBox();
        buttonsBar.setPadding(new Insets(10, 10, 10, 10));
        buttonsBar.setSpacing(10);
        buttonsBar.getChildren().addAll(upgradeButton, deleteButton, detailsButton);

        VBox layout = new VBox();
        layout.getChildren().addAll(table, buttonsBar, newMemberBar);
        Scene scene = new Scene(layout, 600, 350);
        window.setScene(scene);
        window.show();

        logger.log(Level.INFO, "Window is set up");
    }

    public ObservableList<BonusMember> getMembers() {
        ObservableList<BonusMember> members = FXCollections.observableArrayList();
        members.addAll(memberErchive.getArchive());
        return members;
    }

    private void addButtonClicked() {
        logger.log(Level.INFO, "Add Button clicked");
        try {
            int id = memberErchive.addMember(
                    new Personals(firstNameInput.getText(),
                            surnameInput.getText(), emailInput.getText(),
                            passwordInput.getText()), LocalDate.now());
            table.getItems().add(memberErchive.findMember(id));
            logger.log(Level.INFO, "New member added, id: " + id);
            firstNameInput.clear();
            surnameInput.clear();
            passwordInput.clear();
            emailInput.clear();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Exception when adding new member: " + e, e);
        }
    }

    private void deleteButtonClicked() {
        logger.log(Level.INFO, "Delete Button clicked");
        ObservableList<BonusMember> memberSelected, allMembers;
        allMembers = table.getItems();
        memberSelected = table.getSelectionModel().getSelectedItems();

        try {
//            for (BonusMember m : memberSelected) {
//                if (memberErchive.deleteMember(m.getMemberNo())) {
//                    logger.log(Level.INFO, "Member with id " + m.getMemberNo() + " is deleted.");
//                    allMembers.remove(m);
//                }
//            }
//            table.getSelectionModel().clearSelection();
            memberSelected.forEach(e -> {
                if (memberErchive.deleteMember(e.getMemberNo())) {
                    allMembers.remove(e);
                    logger.log(Level.INFO, "Member with id " + e.getMemberNo() + " is deleted.");
                }
            });
            //table.getSelectionModel().clearSelection();
            //table.setItems(getMembers());
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Exception when deleting a member: " + e, e);
        }

    }

    private void upgradeButtonClicked() {
        logger.log(Level.INFO, "Upgrade Button clicked");
        try {
            memberErchive.checkMembers(LocalDate.now());
            table.setItems(getMembers());
            logger.log(Level.INFO, "Memebers upgraded"); // todo change method so that more specific info is given to logger
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Exception when upgrading members: " + e, e);
        }

    }

    private void detailsButtonClicked() {
        logger.log(Level.INFO, "Details Button clicked");
        ObservableList<BonusMember> memberSelected;
        memberSelected = table.getSelectionModel().getSelectedItems();
        memberSelected.forEach(e -> {
            if (e instanceof BasicMember) {
                BasicMemberDetails.display((BasicMember) e);
            } else if (e instanceof SilverMember) {
                SilverMemberDetails.display((SilverMember) e);
            } else if (e instanceof GoldMember) {
                GoldMemberDetails.display((GoldMember) e);
            }
        });
    }

    private void fillRegisterWithMembers() {
        // Add some Basic Members, and register points. Some of the members
        // will be registered with points that qualify them for Silver og Gold
        Personals pers1 = new Personals("Arne", "Styve", "asdf@gmail.no", "qwerty");
        Personals pers2 = new Personals("Ole", "Jensen", "ole@gmail.no", "ytrewq");
        Personals pers3 = new Personals("Lise", "Pedersen", "lise@gmail.no", "zaq123");
        Personals pers4 = new Personals("Kari", "Lid", "kari@gmail.no", "123qaz");
        Personals pers5 = new Personals("Leif", "Øvrebust", "leif@gmail.no", "jadda");

        // Add all persons as members in the archive
        int memberNo1 = memberErchive.addMember(pers1, LocalDate.of(2020, 3, 10));
        memberErchive.registerPoints(memberNo1, 10000);

        int memberNo2 = memberErchive.addMember(pers2, LocalDate.of(2020, 3, 10));
        memberErchive.registerPoints(memberNo2, 30000);

        int memberNo3 = memberErchive.addMember(pers3, LocalDate.of(2020, 3, 10));
        memberErchive.registerPoints(memberNo3, 80000);

        int memberNo4 = memberErchive.addMember(pers4, LocalDate.of(2020, 3, 10));
        memberErchive.registerPoints(memberNo4, 77000);

        int memberNo5 = memberErchive.addMember(pers5, LocalDate.of(2020, 3, 10));
        memberErchive.registerPoints(memberNo5, 35000);
    }
}
